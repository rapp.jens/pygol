##############################################################################
# author       # TecDroiD
# date         # 2023-08-30
# ---------------------------------------------------------------------------
# description  # a continuous variation of conwas GoL
#              #
#              #
##############################################################################
import random
import os
import time
import math
import sys


KERNEL_3X3 = [[1, 1, 1], [1, 0, 1], [1, 1, 1]]


def create_grid(width, height):
    return {
        "width": width,
        "height": height,
        "values": [[0 for i in range(width)] for i in range(height)],
        }


def fill_grid_discrete(grid, prob):
    vals = grid["values"]
    for y in range(grid["height"]):
        for x in range(grid["width"]):
            vals[y][x] = 1 if random.random() < prob else 0


def fill_grid_continuous(grid, prob, pattern_density=0.2):
    vals = grid["values"]
    for y in range(grid["height"]):
        for x in range(grid["width"]):
            if random.random() < pattern_density:
                v = random.uniform(0, 1) if random.random() < prob else 0
                vals[y][x] = v
            else:
                vals[y][x] = 0


def clear_screen():
    # Prüfe das Betriebssystem und führe den passenden Befehl aus
    if os.name == 'posix':  # Unix/Linux/MacOS
        os.system('clear')
    elif os.name == 'nt':  # Windows
        os.system('cls')
    else:
        print("Betriebssystem wird nicht unterstützt.")


def show_grid(grid, iteration=0):
    clear_screen()
    vals = grid["values"]

    print(f"Iteration {iteration}, {grid['width']}:{grid['height']}")

    brightness_levels = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '@']

    for y in range(grid["height"]):
        print()
        for x in range(grid["width"]):
            brightness = int(vals[y][x] * (len(brightness_levels) - 1))
            print(brightness_levels[brightness], end='')
    print()


def run_kernel(grid, x, y, kernel, normalize=True):
    ky = len(kernel)
    kx = len(kernel[0])
    if kx % 2 == 0 or ky % 2 == 0:
        raise ValueError("Kernel size must be odd")

    vals = grid["values"]

    kernel_val = 0
    for dy in range(ky):
        posy = y + dy - ky // 2

        if posy >= 0 and posy < grid["height"]:
            for dx in range(kx):
                posx = x + dx - kx // 2

                if posx >= 0 and posx < grid["width"]:
                    pv = kernel[dy][dx] * (1 if vals[posy][posx] != 0 else 0)
                    kernel_val += pv

    if normalize:
        total_kernel = sum(sum(row) for row in kernel)
        kernel_val = kernel_val / total_kernel

    return kernel_val


def next_iteration(grid, kernel, activation, normalize=True):
    new_grid = create_grid(grid["width"], grid["height"])
    vals = new_grid["values"]

    for y in range(grid["height"]):
        for x in range(grid["width"]):
            kval = run_kernel(grid, x, y, kernel, normalize)
            vals[y][x] = activation(grid["values"][y][x], kval)

    return new_grid


def conway_activation_discrete(current, kernel):
    if current == 1:
        if kernel == 2 or kernel == 3:
            return 1
        else:
            return 0
    elif current == 0:
        if kernel == 3:
            return 1
        else:
            return 0


def conway_activation_continuous(c, kernel):
    current = c
    if current != 0:
        if kernel == 0:
            return 0
        elif kernel <= .2:
            return min(current + 0.1, 1.0)
        elif kernel >= .6:
            return max(current - 0.1, 0.0)
        else:
            return 0
    else:
        if kernel >= .2 and kernel <= 0.6:
            return 1
        else:
            return current


def gaussian_kernel(size, max_value=1):
    if size % 2 == 0:
        raise ValueError("Kernel size must be odd")

    center = size // 2
    kernel = [[0 for _ in range(size)] for _ in range(size)]

    for x in range(size):
        for y in range(size):
            distance = math.sqrt((x - center)**2 + (y - center)**2)
            kernel[x][y] = max_value * math.exp(-distance**2)

    # Normalisieren, um den Maximalwert zu erhalten
    total = sum(sum(row) for row in kernel)
    kernel = [[value / total for value in row] for row in kernel]

    return kernel


if __name__ == "__main__":
    world = create_grid(120, 40)
    kernel = gaussian_kernel(7)
    fill_grid_continuous(world, 0.5)
    activation = conway_activation_continuous
    normalize = True

    if len(sys.argv) > 1 and sys.argv[1] == "discrete":
        kernel = KERNEL_3X3
        fill_grid_discrete(world, 0.5)
        activation = conway_activation_discrete
        normalize = False

    show_grid(world)
    for iteration in range(100):
        time.sleep(.1)
        world = next_iteration(
            world,
            kernel,
            activation,
            normalize=normalize
            )
        show_grid(world, iteration)
