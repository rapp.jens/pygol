##############################################################################
# author       # TecDroiD
# date         # 2023-08-30
# ---------------------------------------------------------------------------
# description  #
#              #
#              #
##############################################################################
import random
import os
import time

KERNAL_3X3=[[1,1,1],[1,0,1],[1,1,1]]

def lenia_activation(current, kernal):
    if current == 1 and kernal > 3 and kernal < 7:
        return 1
    if current == 0 and kernal > 2 and kernal < 8:
        return 1
    return 0

def lenia_kernel(size):
    return [[random.randint(0, 1) for _ in range(size)] for _ in range(size)]

def activation_function_discrete(current, kernal):
    """ this function activates or deactivates a cell by returning the values
    0 or 1
    """
    if current == 1 and kernal > 1 and kernal < 4:
        return 1
    if current == 0 and kernal == 3:
        return 1
    return 0

def activation_function_conways_life(current, kernal):
    if current == 1:
        if kernal == 2 or kernal == 3:
            return 1
        else:
            return 0
    elif current == 0:
        if kernal == 3:
            return 1
        else:
            return 0

def run_kernal(grid, x, y, kernal):
    ky = len(kernal)
    kx = len(kernal[0])
    if kx % 2 == 0 or ky % 2 == 0:
        print("kernal sizes must not be even")
        return

    vals = grid["values"]

    kernal_val = 0
    for dy in range(ky):
        posy = y + dy - ky // 2

        if posy >= 0 and posy < grid["height"]:
            for dx in range(kx):
                posx = x + dx - kx // 2

                if posx >= 0 and posx < grid["width"]:
                    pv = kernal[dy][dx] * vals[posy][posx]
                    kernal_val += pv

    return kernal_val


def create_grid(width, height):
    return {
        "width": width,
        "height": height,
        "values": [[0 for i in range(width)] for i in range(height)],
        }


def random_fill_grid(grid, prob):
    vals = grid["values"]
    for y in range(grid["height"]):
        for x in range(grid["width"]):
            vals[y][x] = 1 if random.random() < prob else 0

def lenia_fill_grid(grid, prob, pattern_density=0.2):
    vals = grid["values"]
    for y in range(grid["height"]):
        for x in range(grid["width"]):
            if random.random() < pattern_density:
                vals[y][x] = 1 if random.random() < prob else 0
            else:
                vals[y][x] = 0


def next_iteration(grid,activation):
    new_grid = create_grid(grid["width"], grid["height"])
    vals = new_grid["values"]

    for y in range(grid["height"]):
        for x in range(grid["width"]):
            kval = run_kernal(grid, x, y, KERNAL_3X3)
            vals[y][x] = activation(grid["values"][y][x], kval)

    return new_grid

def show_grid(grid, iteration=0):
    os.system('clear')
    vals = grid["values"]

    #print(f"{iteration},{grid['width']}:{grid['height']}")

    for y in range(grid["height"]):
        print()
        for x in range(grid["width"]):
            print(f" {vals[y][x]}", end='')


def lenia_activation_continuous(current, kernal):
    if current > 0.5 and kernal > 3 and kernal < 7:
        return 1.0
    if current <= 0.5 and kernal > 2 and kernal < 8:
        return 1.0
    return 0.0


def lenia_kernel_continuous(size):
    return [[random.uniform(0, 1) for _ in range(size)] for _ in range(size)]


def lenia_fill_grid_continuous(grid, prob, pattern_density=0.2):
    vals = grid["values"]
    for y in range(grid["height"]):
        for x in range(grid["width"]):
            if random.random() < pattern_density:
                vals[y][x] = random.uniform(0, 1) if random.random() < prob else 0
            else:
                vals[y][x] = 0


def show_grid_continuous(grid, iteration=0):
    os.system('clear')
    vals = grid["values"]

    print(f"Iteration {iteration}, {grid['width']}:{grid['height']}")

    brightness_levels = [ ' ', '.', ':', 'o', 'O', '0', 'X', '#']

    for y in range(grid["height"]):
        print()
        for x in range(grid["width"]):
            brightness = int(vals[y][x] * (len(brightness_levels) - 1))
            print(brightness_levels[brightness], end='')
    print()


if __name__ == "__main__":
    world = create_grid(60,40)
    kernel = lenia_kernel_continuous(8)
    lenia_fill_grid_continuous(world, 0.6, pattern_density=0.5)

    show_grid_continuous(world)
    for iteration in range(50):
        time.sleep(1)
        world = next_iteration(world, lenia_activation_continuous)
        show_grid_continuous(world, iteration)
